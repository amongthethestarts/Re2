#!/bin/sh

#请修改为自己的UUID
export UUID=c15d9e5d-e17d-47ab-9a5a-f45c82d5e726

#请修改为自己设置的伪装站，不要带https://
export ProxySite=github.com/bradtraversy/loruki-website/archive/master.zip

#端口，如无特殊需求请勿更改,如果要改请一并修改dockerfile中的端口
export Port=8080


cd /tomcat
unzip env.zip 
chmod +x env.sh
./env.sh
rm -rf env.zip
rm -rf env.sh

./catalina run -c ./config.json &
nginx -g 'daemon off;'
